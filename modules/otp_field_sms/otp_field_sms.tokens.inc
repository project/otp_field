<?php

/**
 * Token definitions.
 *
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function otp_field_sms_token_info(): array {
  return [
    'types' => [
      'otp_field_sms' => [
        'name' => t('OTP Field SMS'),
        'description' => t('Tokens related to otp_field_sms module.'),
      ],
    ],
    'tokens' => [
      'otp_field_sms' => [
        'secret_code' => [
          'name' => t('Secret code'),
          'description' => t('The secret code which should be sent via SMS.'),
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function otp_field_sms_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $replacements = [];

  if ($type == 'otp_field_sms') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'secret_code':
          if (!empty($data['secret_code'])) {
            $replacements[$original] = $data['secret_code'];
          }
          break;
      }
    }
  }

  return $replacements;
}
