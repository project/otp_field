<?php

namespace Drupal\otp_field_sms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class OtpFieldSmsSettingsForm extends ConfigFormBase {

  public const CONFIG_NAME = 'otp_field_sms.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'otp_field_sms_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['sms_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('SMS Text'),
      '#description' => $this->t('The SMS text which should be sent to the user. This field supports tokens.'),
      '#config_target' => static::CONFIG_NAME . ':sms_text',
      '#required' => TRUE,
    ];

    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $module_handler */
    $module_handler = \Drupal::service('module_handler');
    if ($module_handler->moduleExists('token')) {
      $form['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [
          'otp_field_sms',
        ],
      ];
    }
    else{
      $form['tokens'] = [
        '#markup' => $this->t('Available tokens include: @tokens.', [
          '@tokens' => implode(', ', [
            '[otp_field_sms:secret_code]',
          ]),
        ]),
      ];
    }

    return parent::buildForm($form, $form_state);
  }
}
